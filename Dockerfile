FROM php:8-apache

# install system dependencies
RUN apt-get update && apt-get install -y git zip unzip

# install php extensions
RUN docker-php-ext-install pdo_mysql

# install composer
RUN curl -sS https://getcomposer.org/installer | \
    php -- --install-dir=/usr/bin/ --filename=composer

COPY . /app
WORKDIR /app
RUN composer install --no-dev --no-interaction -o
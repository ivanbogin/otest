# ochallenge
The challenge is to create a script that will generate a table inside a CSV file with at least the following data:
- Turnover per brand (all brands)
- Turnover per day (of all brands)
- Scope: data should only contain the first 7 days of the month May (01-05-2018 - 07-05-2018).

End result: the end result should be a single table (inside a single CSV file) containing the aforementioned data. CSV needs to be easy to read/analyse for a person on a daily basis.

Bonus: the prices in the database are 21% VAT included. Bonus task is: - Show the excluded VAT per brand as well (21%)

## How to generate the report
* PHP 8 with Composer
    * Make sure `.env` contains proper connection to your database 
    * Run `composer install` to have everything ready 
    * Run `php html/report.php > newreport.csv` to get the report
    * You can also run a local web server with `php -S localhost:8080 -t html/` and visit http://127.0.0.1:8080/report.php
* Alternatively Docker with Docker compose
    * Copy `.env.example` to `.env`
    * Run `docker-compose up -d` to have everything ready
    * Visit http://127.0.0.1:8080/report.php to get the report


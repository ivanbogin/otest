<?php

declare(strict_types=1);

namespace Reports;

use PDO;

use function sprintf;

/**
 * Singleton class to have one single database connection instance.
 */
class Database
{
    private static PDO $instance;

    private function __construct()
    {
    }

    public static function getInstance(): PDO
    {
        if (empty(self::$instance)) {
            $host = $_ENV['DB_HOST'];
            $port = $_ENV['DB_PORT'];
            $db   = $_ENV['DB_NAME'];
            $user = $_ENV['DB_USER'];
            $pass = $_ENV['DB_PASS'];

            $dsn = sprintf('mysql:host=%s;port=%s;dbname=%s', $host, $port, $db);

            self::$instance = new PDO($dsn, $user, $pass);
        }

        return self::$instance;
    }
}

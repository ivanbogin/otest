<?php

declare(strict_types=1);

namespace Reports;

use function round;

class VatCalculator
{
    public const PRECISION = 2;

    public function calculateNet(float $gross, float $vat): float
    {
        if ($vat <= 0) {
            return 0;
        }

        $taxRate  = $vat / 100;
        $taxValue = $gross / (1 + $taxRate) * $taxRate;

        return round($gross - $taxValue, self::PRECISION);
    }
}

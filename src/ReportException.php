<?php

declare(strict_types=1);

namespace Reports;

use RuntimeException;

class ReportException extends RuntimeException
{
}

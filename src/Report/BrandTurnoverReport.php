<?php

declare(strict_types=1);

namespace Reports\Report;

use DateTime;
use League\Csv\AbstractCsv;
use League\Csv\Writer;
use Reports\ReportException;
use Reports\Repository\SalesRepository;
use Reports\VatCalculator;

use function array_merge;
use function array_push;
use function sort;

/**
 * Generates report within the specified date period with the following data:
 * - Turnover per brand (all brands)
 * - Turnover per day (of all brands)
 */
class BrandTurnoverReport
{
    public const VAT = 21;

    protected SalesRepository $salesRepository;

    public function __construct(SalesRepository $salesRepository)
    {
        $this->salesRepository = $salesRepository;
    }

    public function generateCsvReport(string $dateFrom, string $dateTo): AbstractCsv
    {
        $dateFrom = DateTime::createFromFormat('Y-m-d', $dateFrom);
        $dateTo   = DateTime::createFromFormat('Y-m-d', $dateTo);

        if ($dateFrom === false || $dateTo === false) {
            throw new ReportException('Invalid dates format (use Y-m-d)');
        }

        // make sure time is set to 0:00:00
        $dateFrom->setTime(0, 0, 0, 0);
        $dateTo->setTime(0, 0, 0, 0);

        $rows = $this->salesRepository->findBrandsTurnoverByDatesBetween($dateFrom, $dateTo);

        $dates = [];
        $data  = [];

        foreach ($rows as $row) {
            $date     = $row['date'];
            $brand    = $row['name'];
            $turnover = $row['turnover'];

            // fill dictionary of unique dates for columns
            $dates[$date] = $date;

            if (! isset($data[$brand])) {
                // initialize empty list for turnover items
                $data[$brand] = [];
            }

            $data[$brand][$date] = $turnover;
        }

        sort($dates);

        $csv           = Writer::createFromString();
        $vatCalculator = new VatCalculator();

        // prepare header
        $header = array_merge(['brand'], $dates, ['total_incl', 'total_excl']);
        $csv->insertOne($header);

        // prepare records
        $records = [];
        foreach ($data as $brand => $gmv) {
            $record         = [$brand];
            $brandTotalIncl = 0;
            foreach ($dates as $key => $date) {
                $turnover        = $gmv[$date] ?? 0;
                $brandTotalIncl += $turnover;
                $record[]        = $turnover;
            }

            $brandTotalExcl = $vatCalculator->calculateNet($brandTotalIncl, self::VAT);
            array_push($record, $brandTotalIncl, $brandTotalExcl);
            $records[] = $record;
        }

        $csv->insertAll($records);

        return $csv;
    }
}

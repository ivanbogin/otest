<?php

declare(strict_types=1);

namespace Reports\Repository;

use DateTime;

interface SalesRepository
{
    /**
     * Selects turnover of all brands within the specified date period.
     * rows format: name, date, turnover
     *
     * @return array
     */
    public function findBrandsTurnoverByDatesBetween(DateTime $from, DateTime $to): array;
}

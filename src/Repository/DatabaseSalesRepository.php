<?php

declare(strict_types=1);

namespace Reports\Repository;

use DateTime;
use Reports\Database;

class DatabaseSalesRepository implements SalesRepository
{
    /**
     * @inheritdoc
     */
    public function findBrandsTurnoverByDatesBetween(DateTime $from, DateTime $to): array
    {
        $db  = Database::getInstance();
        $sql = 'select brands.name, date(gmv.date) `date`, gmv.turnover
                from gmv
                inner join brands on brands.id = gmv.brand_id
                where gmv.date between :date_from and :date_to
                order by brands.name, gmv.date;';
        $st  = $db->prepare($sql);
        $st->bindValue(':date_from', $from->format('Y-m-d H:i:s'));
        $st->bindValue(':date_to', $to->format('Y-m-d H:i:s'));
        $st->execute();

        return $st->fetchAll();
    }
}

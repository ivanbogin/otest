<?php

declare(strict_types=1);

/**
 * This is a web script which outputs csv into a browser as an attachment.
 */

require __DIR__ . '/../vendor/autoload.php';

use Reports\Report\BrandTurnoverReport;
use Reports\Repository\DatabaseSalesRepository;

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__ . '/..');
$dotenv->load();

$dateFrom = '2018-05-01';
$dateTo   = '2018-05-07';
$fileName = $dateFrom . '-' . $dateTo . '.csv';

header('Content-Type: text/csv; charset=UTF-8');
header('Content-Description: File Transfer');
header('Content-Disposition: attachment; filename="' . $fileName . '"');

$report = new BrandTurnoverReport(new DatabaseSalesRepository());

try {
    $report->generateCsvReport($dateFrom, $dateTo)->output();
} catch (Throwable $e) {
    echo 'Error: ' . $e->getMessage();
}

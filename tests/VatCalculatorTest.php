<?php

declare(strict_types=1);

namespace Tests\Reports;

use PHPUnit\Framework\TestCase;
use Reports\VatCalculator;

class VatCalculatorTest extends TestCase
{
    /**
     * @dataProvider calculateNetProvider
     */
    public function testCalculateNet(float $gross, float $vat, float $net): void
    {
        $vatCalculator = new VatCalculator();
        $this->assertEquals($net, $vatCalculator->calculateNet($gross, $vat));
    }

    /**
     * @return array
     */
    public function calculateNetProvider(): array
    {
        return [
            [0, 20, 0],
            [100, 0, 0],
            [100, 20, 83.33],
            [1209.99, 21, 999.99],
            [10.77, 7.7, 10],
            [100509.98, 21.0, 83066.1],
        ];
    }
}

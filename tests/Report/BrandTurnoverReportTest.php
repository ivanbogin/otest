<?php

declare(strict_types=1);

namespace Tests\Reports\Report;

use PHPUnit\Framework\TestCase;
use Reports\Report\BrandTurnoverReport;
use Reports\ReportException;
use Reports\Repository\SalesRepository;

class BrandTurnoverReportTest extends TestCase
{
    public function testGenerateCsvReportInvalidDate(): void
    {
        $report = new BrandTurnoverReport($this->getMockBuilder(SalesRepository::class)->getMock());

        $this->expectException(ReportException::class);
        $this->expectExceptionMessage('Invalid dates format (use Y-m-d)');
        $report->generateCsvReport('abc', '2018-05-07');

        $this->expectException(ReportException::class);
        $this->expectExceptionMessage('Invalid dates format (use Y-m-d)');
        $report->generateCsvReport('2018-05-07', '');
    }

    public function testGenerateCsvReport(): void
    {
        $salesData       = [
            ['name' => 'I-Brand', 'date' => '2018-05-01', 'turnover' => 100],
            ['name' => 'I-Brand', 'date' => '2018-05-03', 'turnover' => 100.33],
            ['name' => 'O-Brand', 'date' => '2018-05-02', 'turnover' => 200],
            ['name' => 'O-Brand', 'date' => '2018-05-03', 'turnover' => 200.33],
            ['name' => 'O-Brand', 'date' => '2018-05-04', 'turnover' => 200.44],
            ['name' => 'R-Brand', 'date' => '2018-05-01', 'turnover' => 300],
        ];
        $salesRepository = $this->getMockBuilder(SalesRepository::class)->getMock();
        $salesRepository->method('findBrandsTurnoverByDatesBetween')->willReturn($salesData);

        $report = new BrandTurnoverReport($salesRepository);
        $csv    = $report->generateCsvReport('2018-05-01', '2018-05-07');

        $expected = "brand,2018-05-01,2018-05-02,2018-05-03,2018-05-04,total_incl,total_excl
I-Brand,100,0,100.33,0,200.33,165.56
O-Brand,0,200,200.33,200.44,600.77,496.5
R-Brand,300,0,0,0,300,247.93\n";

        $this->assertEquals($expected, (string) $csv);
    }
}
